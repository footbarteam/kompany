import angular from 'angular';

import 'angular-ui-router';
import routesConfig from './routes';
import 'angular-translate';
import 'angular-cookies';
import 'angular-ui-bootstrap';
import translateConfig from './translations';
import 'numeral/locales/fr';
import 'angular-resource';
import 'angular-moment';
import 'satellizer';
import ngInfiniteScroll from 'ng-infinite-scroll';
import 'angular-loading-bar';
import 'ng-file-upload';
import 'ng-tags-input';
import 'angulartics';
import 'angulartics-google-analytics';

import {rootComponent} from './app/root';
import {App} from './app/containers/App';
import {SessionDetail} from './app/containers/SessionDetail';
import {ProfileDetail} from './app/containers/ProfileDetail';
import {MyProfileDetail} from './app/containers/MyProfileDetail';
import {Leaderboard} from './app/containers/Leaderboard';
import {Worldcup} from './app/containers/Worldcup';
import {GameList} from './app/containers/GameList';
import {GameDetail} from './app/containers/GameDetail';
import {SkillTests} from './app/containers/SkillTests';
import {CoachView} from './app/containers/CoachView';
import {Header} from './app/components/Header';
import {LineUpPlayer} from './app/components/LineUpPlayer';
import {LineUpLine} from './app/components/LineUpLine';
import {FormatNumber} from './app/components/NumeralFilter';
import {FormatCountry} from './app/components/CountryFilter';
import {FootbarRequestManager} from './app/api/RequestManager';
import {OkochaAPIService} from './app/api/okocha';
import {FabinhoAPIService} from './app/api/fabinho';
import {CantonaAPIService} from './app/api/cantona';
import {AlertService} from './app/components/alert';
import {FootbarAuthService} from './app/components/FootbarAuth';
import {LoginModal} from './app/components/LoginModal';
import {SignupModal} from './app/components/SignupModal';
import {ProfileUpdateModal} from './app/components/ProfileUpdateModal';
import {ProfileMergeModal} from './app/components/ProfileMergeModal';
import {GameCreateModal} from './app/components/GameCreateModal';
import {TrainingGroupCreateModal} from './app/components/TrainingGroupCreateModal';
import {LeaderboardListItem} from './app/components/LeaderboardListItem';

import './index.less';

/** Satellizer conf. */
/** @ngInject */
const authConfig = $authProvider => {
  const authUrl = `${process.env.PROTOCOL}${process.env.OKOCHA_DOMAIN}/rest-auth/`;
  $authProvider.loginUrl = `${authUrl}login/`;
  $authProvider.signupUrl = `${authUrl}registration/`;
  $authProvider.tokenType = 'Token';
  $authProvider.facebook({
    clientId: process.env.FACEBOOK_APP,
    url: `${authUrl}facebook/`
  });
};

/** Conf of $resource. */
/** @ngInject */
const resourceConfig = $resourceProvider => {
  $resourceProvider.defaults.stripTrailingSlashes = false;
};

/** Loading bar conf. */
/** @ngInject */
const loadingBarConfig = cfpLoadingBarProvider => {
  cfpLoadingBarProvider.includeSpinner = false;
  cfpLoadingBarProvider.parentSelector = '#navigation';
};

angular
  .module('app', [
    'ui.router', 'ui.bootstrap', 'pascalprecht.translate', 'ngCookies', 'ngResource', 'angularMoment',
    'satellizer', ngInfiniteScroll, 'angular-loading-bar', 'ngFileUpload', 'ngTagsInput',
    'angulartics', 'angulartics.google.analytics'
  ])
  .config(routesConfig)
  .config(translateConfig)
  .config(resourceConfig)
  .config(authConfig)
  .config(loadingBarConfig)
  .component('root', rootComponent)
  .component('app', App)
  .component('sessionDetail', SessionDetail)
  .component('profileDetail', ProfileDetail)
  .component('myProfileDetail', MyProfileDetail)
  .component('leaderboard', Leaderboard)
  .component('worldcup', Worldcup)
  .component('gameList', GameList)
  .component('gameDetail', GameDetail)
  .component('skillTests', SkillTests)
  .component('coachView', CoachView)
  .component('headerComponent', Header)
  .component('lineUpPlayer', LineUpPlayer)
  .component('lineUpLine', LineUpLine)
  .component('loginModal', LoginModal)
  .component('signupModal', SignupModal)
  .component('profileUpdateModal', ProfileUpdateModal)
  .component('profileMergeModal', ProfileMergeModal)
  .component('gameCreateModal', GameCreateModal)
  .component('trainingGroupCreateModal', TrainingGroupCreateModal)
  .component('leaderboardListItem', LeaderboardListItem)
  .filter('formatNumber', FormatNumber)
  .filter('formatCountry', FormatCountry)
  .service('requestManager', FootbarRequestManager)
  .service('okochaAPIService', OkochaAPIService)
  .service('fabinhoAPIService', FabinhoAPIService)
  .service('cantonaAPIService', CantonaAPIService)
  .service('alertService', AlertService)
  .service('footbarAuthService', FootbarAuthService);
