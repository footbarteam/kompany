export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  //  According to Footbar policy in README, url must be defined with trailing slash.
  $stateProvider
    .state('root', {
      component: 'root',
      url: '/{lang:(?:fr|en)}',
      params: {  // French and english are allowed, default to english.
        lang: {
          squash: true, value: 'en', dynamic: true
        }
      },
      abstract: true
    })
    .state('app', {
      url: '/',
      component: 'app',
      parent: 'root'
    })
    .state('sessionDetail', {
      url: '/session/:id/',
      component: 'sessionDetail',
      parent: 'root'
    })
    .state('profileDetail', {
      url: '/profile/:id/',
      component: 'profileDetail',
      parent: 'root'
    })
    .state('myProfileDetail', {
      url: '/myprofile/',
      component: 'myProfileDetail',
      parent: 'root'
    })
    .state('leaderboard', {
      url: '/leaderboard/',
      component: 'leaderboard',
      parent: 'root'
    })
    .state('worldcup', {
      url: '/worldcup/',
      component: 'worldcup',
      parent: 'root'
    })
    .state('coach', {
      url: '/coach/',
      component: 'coachView',
      parent: 'root'
    })
    .state('gameList', {
      url: '/game/list/',
      component: 'gameList',
      parent: 'root'
    })
    .state('gameDetail', {
      url: '/game/detail/:id/',
      component: 'gameDetail',
      parent: 'root'
    })
    .state('skillTests', {
      url: '/skill_tests/:id/',
      component: 'skillTests',
      parent: 'root'
    });
}
