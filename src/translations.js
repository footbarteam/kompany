import countries from 'i18n-iso-countries';

export default translateConfig;

/** @ngInject */
function translateConfig($translateProvider) {
  // Load countries names.
  countries.registerLocale(require('i18n-iso-countries/langs/en.json'));
  countries.registerLocale(require('i18n-iso-countries/langs/fr.json'));

  $translateProvider
    .useSanitizeValueStrategy('escape')
    .translations('en', {
      GENERAL: {
        NAVBAR: {
          TOGGLE: 'Toggle navigation',
          TOGGLE_LANG: 'Toggle language',
          FR: 'French version',
          EN: 'English version',
          LEADERBOARD: 'Leaderboard',
          WORLDCUP: 'Worldcup',
          GAME_LIST: 'All games',
          PROFILE: 'Profile',
          ACCOUNT: 'Connect to your account',
          MY_ACCOUNT: 'My profile'
        },
        REMOTE_ERROR: 'Error fetching data. Please retry later.',
        NOT_LOGGED_ERROR: 'You must be logged in to access this page.',
        MODAL: {
          TITLE_LOGIN: '@:GENERAL.NAVBAR.ACCOUNT',
          TITLE_LOGOUT: 'You are already logged in',
          TITLE_SIGNUP: 'Create a Footbar account',
          CONNECT: 'Connect',
          DISMISS: 'Cancel',
          CREATE_ACCOUNT: 'Create a new account',
          LOGOUT: 'Logout',
          WITH_FACEBOOK: 'With Facebook',
          WITH_AN_EMAIL: 'With an email',
          FORGOT_PASSWORD: 'Forgot password',
          CONFIRM_PASSWORD: 'Confirm your password',
          CONFIRM: 'Confirm',
          LOG_EXISTING_ACCOUNT: 'I already have an account',
          TITLE_UPDATE_STATS: 'Update my stats',
          BODY_UPDATE_STATS: 'If you have played recently, click on this button to tell the platform to fetch your new performance data.',
          CTA_UPDATE_STATS: 'Update my stats',
          CANNOT_UPDATE_STATS: 'You already asked for updating your stats. Please wait, they will be available on the platform soon.',
          TITLE_UPDATE_PROFILE: 'Some information to make your profile nicer!',
          CTA_UPDATE_PROFILE: 'Update my profile',
          TITLE_MERGE_PROFILE: 'Did you play with the tracker already?',
          BODY_MERGE_PROFILE: 'If you have worn your tracker, your stats are somewhere in our database. Use this search engine to find it by entering the name under which you have been registered while you played. When you\'re done, click on ask for merge. When we will have proceeded with verification, your stats will appear on your profile.',
          CTA_MERGE_PROFILE: 'Ask for merge',
          CANCEL_MERGE_PROFILE: 'I\'ve never played with a tracker',
          TYPE_PLAYER: 'Type the name under which you played',
          TITLE_PLAYER_CREATE: 'Final step! Get into the Footbar City App!',
          BODY_PLAYER_CREATE: 'We need to fetch your previous stats to compare it to the City players. After a few seconds your new Citizen profile will be complete and you will compare yourself to all the fans!',
          CTA_PLAYER_CREATE: 'Load my data',
          SAVE: 'Save'
        },
        USER: {
          EMAIL: 'Email',
          PASSWORD: 'Password',
          FIRST_NAME: 'First Name',
          LAST_NAME: 'Last Name',
          COUNTRY: 'Country',
          CITY: 'City',
          PROFILE_PIC: 'Profile picture',
          CHOOSE_PIC: 'Select a nice picture'
        }
      },
      HOME: {
        TITLE: "Manchester City Footbar App",
        DESCRIPTION: "Anytime you play with your Meteor Tracker, analyze your performance and compare yourself to Manchester City's professional roster."
      },
      WORLDCUP: {
        TITLE: 'City World Cup'
      },
      LEADERBOARD: {
        TITLE: 'Leaderboard',
        AS: 'as',
        NO_PROFILES: 'No players yet!'
      },
      PROFILE: {
        DOPPELGANGER: 'Doppelganger',
        EMPTY_DOPPELGANGER: 'As soon as you have stats!',
        SESSIONS: 'Sessions',
        ROOKIE: 'Rookie',
        NO_SESSIONS: 'No sessions yet!'
      },
      SESSION: {
        TOTAL: 'Total',
        STYLE_OF: 'in the style of',
        SPEED: 'Speed',
        STAMINA: 'Stamina',
        DRIBBLING: 'Dribbling',
        DEFENDING: 'Defending',
        PASSING: 'Passing',
        SHOOTING: 'Shooting',
        PEPSEYE: 'Pep\'s eye :',
        STYLE: {
          TITLE: {
            0: 'Old style defender',
            1: 'Basketball player turned into a forward',
            2: 'Defender by default',
            3: 'Box to box midfielder',
            4: 'Flying winger',
            5: 'Quick defender',
            6: 'Sluggish defender',
            7: 'Winger in his dreams',
            8: 'Frustrating finisher',
            9: 'Midfield general',
            10: 'The magician',
            11: 'Out of shape player',
            12: 'The butcher of sunday league',
            13: 'Thinks is the star forward',
            14: 'Low stamina striker',
            15: 'The defender who loves to go forward'
          },
          BODY: {
            0: 'An old fashioned type of defender. What he lacks in speed he makes up for in positioning and game understanding.',
            1: 'Not very mobile but great in the air. Uses his feet to stand on, that\'s all.',
            2: 'Never really had any skill with his feet, we put him as defender to limit the damage.',
            3: 'He\'s got it all, from defending to attacking, not really fast but tough.',
            4: 'He strikes fear in the defense. Can upset his teammates because he usually prefers to shoot and miss rather than making a good pass.',
            5: 'Average player but his speed can save him.',
            6: 'Average player but has got no pace.',
            7: 'Doesn\'t have the dribbling skills, the speed, or agility to be the winger he sees on TV. Too bad.',
            8: 'Forward that does everything correctly or nothing well, up to you.',
            9: 'Plays as number 6 or as a wing back. Never stops running.',
            10: 'He can get out of any situation, you can\'t contain him.',
            11: 'Gets back to the field after long term injury, disappears after 15 minutes.',
            12: 'Will never last 90 minutes. In love with the adversary ankles.',
            13: 'The star of the team, loves to dribble even in his own box.',
            14: 'He\'s got the skills but not the engine.',
            15: 'Modern style defender who loves to go forward but lacks in defensive skills.'
          }
        }
      },
      GAME: {
        PICK_GAME: 'Pick a game',
        START_TIME: 'Beginning:',
        STOP_TIME: 'End:',
        TYPE: 'Type:',
        EDIT_TITLE: 'Games of ',
        REG_SESSIONS: 'Registered sessions:',
        NEW_SESSIONS: 'New session:',
        EDIT_SESSIONS: 'Edit sessions:',
        ADD_PLAYER: 'Add new player',
        ADD_ALL_PLAYERS: 'Add all regular players',
        ADD_SESSION: 'Add new session',
        TYPES: {
          TR: 'Training',
          ST: 'Skills test',
          FV: 'Small sided game',
          11: '11v11 game'
        },
        RANKING: 'Ranking',
        NAME: 'Name:',
        STATS: {
          TOT: 'TOT:',
          PAC: 'PAC:',
          STA: 'STA:',
          DRI: 'DRI:',
          DEF: 'DEF:',
          PAS: 'PAS:',
          SHO: 'SHO:',
          SPD: 'Sprint speed:',
          SCT: 'Sprint count:',
          DIS: 'Distance:',
          ACT: 'Activity:',
          SAG: 'Stop and go:',
          PCT: 'Pass count:',
          SHC: 'Shout count:',
          SSP: 'Max shot speed:'
        }
      },
      TRAINING_GROUP: {
        EDIT: 'Edit training group',
        NAME: 'Name',
        FAV_TRACKER: 'Favorite tracker',
        ADD_PLAYER: 'Add new player',
        FULL_TRACKERS: 'Full trackers list'
      }
    })
    .translations('fr', {
      GENERAL: {
        NAVBAR: {
          TOGGLE: 'Ouvrir le menu',
          TOGGLE_LANG: 'Changer de langue',
          TITLE_SIGNUP: 'Création d\'un compte Footbar',
          FR: 'Version française',
          EN: 'Version anglaise',
          LEADERBOARD: 'Classement général',
          WORLDCUP: 'Coupe du monde',
          GAME_LIST: 'Tous les matchs',
          PROFILE: 'Profil',
          ACCOUNT: 'Se connecter',
          MY_ACCOUNT: 'Mon profil'
        },
        REMOTE_ERROR: 'Erreur de récupération des données. Veuillez réessayer ultérieurement.',
        NOT_LOGGED_ERROR: 'Vous devez être connecté pour accéder à cette page.',
        MODAL: {
          TITLE_LOGIN: '@:GENERAL.NAVBAR.ACCOUNT',
          TITLE_LOGOUT: 'Vous êtes déjà connecté',
          CONNECT: 'Connexion',
          DISMISS: 'Annuler',
          CREATE_ACCOUNT: 'Créer un compte',
          LOGOUT: 'Se déconnecter',
          WITH_FACEBOOK: 'Avec Facebook',
          WITH_AN_EMAIL: 'Avec un email',
          FORGOT_PASSWORD: 'Mot de passe oublié',
          CONFIRM_PASSWORD: 'Confirmez le mot de passe',
          CONFIRM: 'Valider',
          LOG_EXISTING_ACCOUNT: 'J\'ai déjà un compte',
          TITLE_UPDATE_STATS: 'Mettre à jour mes stats',
          BODY_UPDATE_STATS: 'Si vous avez joué récemment, cliquez sur ce bouton afin que la plateforme récupère vous récentes performances.',
          CTA_UPDATE_STATS: 'Mettre à jour mes stats',
          CANNOT_UPDATE_STATS: 'Vous avez déjà demandé à mettre à jour vos stats. Merci de patienter, elles seront disponibles sur la plateforme rapidement.',
          TITLE_UPDATE_PROFILE: 'Quelques infos pour un profil plus sylé !',
          CTA_UPDATE_PROFILE: 'Mettre à jour mon profil',
          TITLE_MERGE_PROFILE: 'Avez-vous déjà porté le tracker ?',
          BODY_MERGE_PROFILE: 'Si vous avez déjà porté votre tracker, vos stats sont quelque part dans notre base de données. Merci d\'utiliser le moteur de recherche suivant afin de les retrouver, en entrant le nom sous lequel vous avez été enregistré quand vous avez joué. Quand ce sera fait, cliquez sur demande de regroupement. Une fois que nous aurons procédé aux vérifications, vos stats apparaîtront sur votre profil.',
          CTA_MERGE_PROFILE: 'Demande de regroupement',
          CANCEL_MERGE_PROFILE: 'Je n\'ai jamais porté le tracker',
          TYPE_PLAYER: 'Entrez le nom sous lequel vous avez joué',
          TITLE_PLAYER_CREATE: 'Dernière étape ! Entrez dans la Footbar City App !',
          BODY_PLAYER_CREATE: 'Nous avons besoin de récupérer vos stats passées afin de vous comparer aux joueurs de City. D\'ici quelques secondes votre nouveau profil de Citizen sera complet et vous pourrez vous comparer à tous les supporters !',
          CTA_PLAYER_CREATE: 'Charger mes données',
          SAVE: 'Sauvegarder'
        },
        USER: {
          EMAIL: 'Email',
          PASSWORD: 'Mot de passe',
          FIRST_NAME: 'Prénom',
          LAST_NAME: 'Nom',
          COUNTRY: 'Pays',
          CITY: 'Ville',
          PROFILE_PIC: 'Photo de profil',
          CHOOSE_PIC: 'Choisissez une photo sympa'
        }
      },
      HOME: {
        TITLE: "Manchester City Footbar App",
        DESCRIPTION: "C'est qui le meilleur supporter de City ? À chaque fois que tu fais un match avec ton Tracker Meteor, compare toi aux joueurs de l'équipe pro et représente ton club dans la Coupe du Monde de City !"
      },
      WORLDCUP: {
        TITLE: 'Coupe du monde city'
      },
      LEADERBOARD: {
        TITLE: 'Classement général',
        AS: 'pour',
        NO_PROFILES: 'Pas encore de joueurs !'
      },
      PROFILE: {
        DOPPELGANGER: 'Sosie',
        EMPTY_DOPPELGANGER: 'Dès que vous aurez des stats !',
        SESSIONS: 'Sessions',
        ROOKIE: 'Recrue',
        NO_SESSIONS: 'Pas encore de sessions !'
      },
      SESSION: {
        TOTAL: 'Total',
        STYLE_OF: 'dans le style de',
        SPEED: 'Vitesse',
        STAMINA: 'Endurance',
        DRIBBLING: 'Dribbe',
        DEFENDING: 'Défense',
        PASSING: 'Passe',
        SHOOTING: 'Frappe',
        PEPSEYE: 'L\'oeil de Pep :',
        STYLE: {
          TITLE: {
            0: 'Défenseur à l’ancienne',
            1: 'Basketteur reconverti en attaque',
            2: 'Défenseur malgré lui',
            3: 'Milieu certifié babtou solide',
            4: 'Ailier casseur de reins',
            5: 'Défenseur moyen mais rapide',
            6: 'Défenseur moyen mais très lent',
            7: 'Ailier à peu près',
            8: 'Attaquant quelconque (comprendre : pas de réelle qualité)',
            9: 'Homme aux 3 poumons',
            10: 'Joueur de petits espaces',
            11: 'Joueur hors de forme',
            12: 'Boucher du dimanche matin',
            13: 'Attaquant qui se prend pour une star',
            14: 'Attaquant sans cardio',
            15: 'Défenseur / latéral trop gentil'
          },
          BODY: {
            0: 'Défenseur qui compense sa vitesse de tortue myopathe par une science du placement et une anticipation sans faille.',
            1: 'Attaquant qui se sert le moins souvent possible de ses pieds. Heureusement pour lui, il sait jouer des coudes pour gagner les duels aériens.',
            2: 'N’ayant pas vraiment de qualité balle au pied, ce joueur s’est retrouvé en défense, ou parfois en 6. Ca limite la casse.',
            3: 'Milieu à tout faire capable de défendre comme d’attaquer. Pas forcément rapide mais difficile à bouger.',
            4: 'Joueur capable de mettre le feu dans une défense. Enerve souvent ses coéquipiers par ses mauvais choix et ses frappes dans les nuages.',
            5: 'Défenseur d’un niveau correct, mais qui s’arrache à prix d’or sur FUT. Merci la vitesse !',
            6: 'Défenseur d’un niveau correct, mais que personne ne prend sur FUT. Pas merci la lenteur !',
            7: 'Aimerait être capable de mettre le feu à une défense. Malheureusement, ce joueur manque de vitesse, de qualité de dribble et d’agilité. De tout quoi. Dommage.',
            8: 'Attaquant sachant tout faire correctement. Ou attaquant ne sachant rien faire d’exceptionnel. A vous de voir.',
            9: 'Joueur infatigable donnant régulièrement l’impression de jouer avec son jumeau sur le terrain. Généralement latéral ou 6.',
            10: 'Joueur à la technique soyeuse capable de ressortir n’importe quel ballon proprement. Cauchemar pour le pressing adverse.',
            11: 'Joueur reprenant le foot après les croisés. Disparaît dès la 15ème minute.',
            12: 'Défenseur pour qui un match dure rarement 90min. Voue un amour sans limite pour les chevilles adverses.',
            13: 'La starlette de l’équipe. Geste technique préféré ? Les passements de jambe. À 70m des buts adverses.',
            14: 'Attaquant sachant faire illusion une mi-temps. Ça peut suffire pour partir en PremierLeague.',
            15: 'Le défenseur moderne capable de tout faire : relancer, courir, dribbler. Sauf défendre.'
          }
        }
      },
      GAME: {
        PICK_GAME: 'Choisissez un match',
        START_TIME: 'Début :',
        STOP_TIME: 'Fin :',
        TYPE: 'Type :',
        EDIT_TITLE: 'Matchs du ',
        REG_SESSIONS: 'Session planifiées :',
        NEW_SESSIONS: 'Nouvelle session :',
        EDIT_SESSIONS: 'Modifier les sessions :',
        ADD_PLAYER: 'Ajouter un nouveau joueur',
        ADD_ALL_PLAYERS: 'Ajouter tous les joueurs réguliers',
        ADD_SESSION: 'Ajouter une nouvelle session',
        TYPES: {
          TR: 'Entraînement',
          ST: 'Tests d\'aptitude',
          FV: 'Match terrain réduit',
          11: 'Match à 11'
        },
        RANKING: 'Classement',
        NAME: 'Nom :',
        STATS: {
          TOT: 'TOT :',
          PAC: 'VIT :',
          STA: 'END :',
          DRI: 'DRI :',
          DEF: 'DEF :',
          PAS: 'PAS :',
          SHO: 'TIR :',
          SPD: 'Vitesse de sprint :',
          SCT: 'Nombre de sprints :',
          DIS: 'Distance :',
          ACT: 'Activité :',
          SAG: 'Changements de rythme :',
          PCT: 'Nombre de passes :',
          SHC: 'Nombre de ballons longs :',
          SSP: 'Vitesse de frappe max :'
        }
      },
      TRAINING_GROUP: {
        EDIT: 'Modifier le groupe',
        NAME: 'Nom',
        FAV_TRACKER: 'Tracker favori',
        ADD_PLAYER: 'Ajouter un nouveau joueur',
        FULL_TRACKERS: 'Liste complète des trackers'
      }
    })
    .registerAvailableLanguageKeys(['en', 'fr'], {
      'en_*': 'en',
      'fr_*': 'fr'
    })
    .fallbackLanguage('en')
    .determinePreferredLanguage();
}
