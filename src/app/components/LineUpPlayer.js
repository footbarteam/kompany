class LineUpPlayerController {
  /** @ngInject */
  constructor($log) {
    // Bind useful services.
    this.log = $log.log;
  }
}

export const LineUpPlayer = {
  template: require('./LineUpPlayer.html'),
  controller: LineUpPlayerController,
  bindings: {
    player: '<'
  }
};
