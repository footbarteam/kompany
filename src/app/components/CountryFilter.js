/**
 * Simple filter to retrieve the name of the country in the good locale.
 */
import countries from 'i18n-iso-countries';

export function FormatCountry() {
  return (code, locale) => countries.getName(code, locale);
}
