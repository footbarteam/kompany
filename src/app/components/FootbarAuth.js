/**
 * Service enabling Footbar authentication.
 */
export class FootbarAuthService {
  /** @ngInject **/
  constructor($auth, $uibModal, $log, $state, okochaAPIService, fabinhoAPIService, $q, $rootScope) {
    // Attach useful services.
    this.$auth = $auth;
    this.uibModal = $uibModal;
    this.log = $log.log;
    this.$state = $state;
    this.okocha = okochaAPIService;
    this.fabinho = fabinhoAPIService;
    this.$q = $q;

    // Don't overuse $rootScope, juste for event handling.
    this.userUpdated = newUser => {
      this.user = newUser;
      $rootScope.$broadcast('userUpdated');
    };
    this.userConnected = () => $rootScope.$broadcast('userConnected');
    this.userDisconnected = () => $rootScope.$broadcast('userDisconnected');

    // Define user object (empty by default).
    // Store the promise to allow using it outside and wait for user to have been retrieved.
    this.userPromise = this.setUserInfo();
  }

  /**
   * Get authentication status.
   */
  get isAuthenticated() {
    return this.$auth.isAuthenticated();
  }

  /**
   * Wrapper around buidling modal.
   */
  openModal(theme, backdrop = true) {
    // Create the modal and return the promise of resolution.
    return this.uibModal.open({
      animation: true,
      backdrop,
      component: theme
    }).result;
  }

  /**
   * Shortcut to set token and store user object.
   */
  login(token) {
    this.$auth.setToken(token);
    return this.setUserInfo();
  }

  /**
   * Logout User and redirect to main page.
   */
  logout() {
    delete this.user;
    delete this.profile;
    this.$auth.logout();
    this.userDisconnected();
  }

  /**
   * Set user and it's okocha profile.
   * Return a promise to make sure all is done.
   */
  setUserInfo() {
    if (this.isAuthenticated) {
      return this.okocha.getUser()
        .then(user => {
          this.user = user;
          if (!this.user.first_name && !this.user.last_name) {
            return this.openProfileUpdateModal();
          }
        })
        .catch(() => {})
        .then(() => {
          if (this.user.needs_merge) {
            return this.openProfileMergeModal();
          }
        })
        .then(() => this.okocha.getProfileDetail({user_id: this.user.id}))  // eslint-disable-line camelcase
        .then(profile => {
          this.profile = profile;
          return this.fabinho.getPlayerDetail({okocha_id: this.profile.id});  // eslint-disable-line camelcase
        })
        .then(player => {
        //   // In case the player was not already existing, create it.
        //   if (!player.okocha_id) {
        //     return this.fabinho.createPlayer(this.profile.id);  // eslint-disable-line camelcase
        //   }
        //   return player;
        // })
        // .then(player => {
          this.player = player;
          this.userConnected();
        });
    }
    // Default to empty promise.
    return this.$q.when();
  }

  /**
   * Manual way of retriggering fetching of user info.
   * Can be used in you controller if you want the user object to be fetched again.
   * It is assumed that user is logged in, otherwise it will fail anyway.
   */
  triggerUserUpdate() {
    return this.okocha.getUser()
      .then(user => this.userUpdated(user));
  }

  /**
   * Open a modal proposing to merge profile.
   */
  openProfileMergeModal() {
    return this.openModal('profileMergeModal');
  }

  /**
   * Open a modal proposing to connect.
   */
  openConnectionModal() {
    return this.openModal('loginModal');
  }

  /**
   * Open a modal proposing to signup.
   */
  openSignupModal() {
    return this.openModal('signupModal');
  }

  /**
   * Open a modal proposing to update profile info.
   */
  openProfileUpdateModal() {
    return this.openModal('profileUpdateModal');
  }
}
