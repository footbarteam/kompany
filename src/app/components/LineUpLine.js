class LineUpLineController {
  /**
   * Deal only when bindings are ready.
   */
  $onChanges(changes) {
    if (changes.hasOwnProperty('players') && this.players) {
      // Set offensive attribute to first and last players of line.
      if (this.players.length > 2) {
        this.players[0].offensive = true;
        this.players[this.players.length - 1].offensive = true;
      }
    }
  }

  get width() {
    return `${Math.min(100 / this.players.length, 25)}%`;
  }

  /**
   * Reverse or not the player list depending on if the composition is inverted.
   */
  get playerList() {
    return (this.inverted) ? this.players.slice().reverse() : this.players;
  }
}

export const LineUpLine = {
  template: require('./LineUpLine.html'),
  controller: LineUpLineController,
  bindings: {
    players: '<',
    inverted: '<'
  }
};
