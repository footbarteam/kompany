/**
 * Modal handling user login.
 */
class LoginModalController {
  /** @ngInject */
  constructor($log, $auth, alertService, $translate, footbarAuthService, $scope, $uibModalStack) {
    this.$auth = $auth;
    this.auth = footbarAuthService;
    this.alert = alertService;
    // Remove any alerts that could have been created by another component.
    this.alert.close();
    this.log = $log.log;
    this.modalStack = $uibModalStack;

    // Fetch strings.
    $translate(['GENERAL.REMOTE_ERROR'])
      .then(translations => {
        this.translations = translations;
      });

    // Close when user is connected.
    $scope.$on('userConnected', () => {
      this.close();
    });
  }

  /**
   * Login the user, no need to close the modal as signal will do it.
   */
  login() {
    this.errors = [];
    return this.$auth.login(this.user)
      .then(response => {
        return this.auth.login(response.data.key);
      })
      .catch(error => {
        // Deal with case of error not explicited.
        if (!error.data) {
          this.alert.add('danger', this.translations['GENERAL.REMOTE_ERROR']);
          this.log(error);
          return;
        }

        // Store errors to display in form.
        this.errors = error.data;

        // Add non field errors to top of page.
        if (angular.isDefined(this.errors.non_field_errors)) {
          this.errors.non_field_errors.map(nfError => this.alert.add('danger', nfError));
        }
      });
  }

  /**
   * Facebook login, no need to close the modal as signal will do it.
   */
  facebook() {
    return this.$auth.authenticate('facebook')
      .then(response => {
        return this.auth.login(response.data.key);
      })
      .catch(error => {
        this.alert.add('danger', this.translations['GENERAL.REMOTE_ERROR']);
        this.log(error);
      });
  }

  /**
   * Find out if modal is closable. Maybe there is more elegant.
   */
  get disableClose() {
    return this.modalStack.getTop() ? (this.modalStack.getTop().value.backdrop === 'static') : false;
  }
}

export const LoginModal = {
  template: require('./LoginModal.html'),
  controller: LoginModalController,
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  }
};
