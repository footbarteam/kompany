/**
 * The number library had no built in filter to format number, let's create one.
 */
import numeral from 'numeral';

export function FormatNumber() {
  return (value, format) => numeral(value).format(format);
}
