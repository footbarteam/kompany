class LeaderboardListItemController {
  /** @ngInject */
  constructor($log, okochaAPIService) {
    this.log = $log.log;
    this.okochaAPI = okochaAPIService;
  }

  /**
   * Deal with initialized controller.
   */
  $onInit() {
    // So far this is too slow to rely on it.
    // this.okochaAPI.getExportedProfileDetail({id: this.player.okocha_id})
    //   .then(profile => {
    //     this.profile = profile;
    //   });
  }
}

export const LeaderboardListItem = {
  template: require('./LeaderboardListItem.html'),
  controller: LeaderboardListItemController,
  bindings: {
    player: '<',
    ranking: '<'
  }
};
