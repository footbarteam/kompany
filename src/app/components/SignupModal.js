/**
 * Modal handling user signup.
 */
class SignupModalController {
  /** @ngInject */
  constructor($log, $auth, alertService, $translate, footbarAuthService, $scope) {
    this.$auth = $auth;
    this.auth = footbarAuthService;
    this.alert = alertService;
    // Remove any alerts that could have been created by another component.
    this.alert.close();
    this.log = $log.log;

    // Fetch strings.
    $translate(['GENERAL.REMOTE_ERROR'])
      .then(translations => {
        this.translations = translations;
      });

    // Close when user is connected.
    $scope.$on('userConnected', () => {
      this.close();
    });
  }

  /**
   * Signup the user, close the modal if ok.
   */
  signup() {
    this.errors = [];
    return this.$auth.signup(this.user)
      .then(response => {
        return this.auth.login(response.data.key);
      })
      .catch(error => {
        // Deal with case of error not explicited.
        if (!error.data) {
          this.alert.add('danger', this.translations['GENERAL.REMOTE_ERROR']);
          this.log(error);
          return;
        }
        // If the mail is already taken, propose to reset password.
        if (angular.isDefined(error.data.email) && error.data.email.some(mailError => [
          'A user is already registered with this e-mail address.',
          'Un autre utilisateur utilise déjà cette adresse e-mail.'
        ].includes(mailError))) {
          error.data.resetMail = true;
        }

        // Store errors to display in form.
        this.errors = error.data;

        // Add non field errors to top of page.
        if (angular.isDefined(this.errors.non_field_errors)) {
          this.errors.non_field_errors.map(nfError => this.alert.add('danger', nfError));
        }
      });
  }

  /**
   * Facebook signup.
   */
  facebook() {
    return this.$auth.authenticate('facebook')
      .then(response => {
        return this.auth.login(response.data.key);
      })
      .catch(error => {
        this.alert.add('danger', this.translations['GENERAL.REMOTE_ERROR']);
        this.log(error);
      });
  }
}

export const SignupModal = {
  template: require('./SignupModal.html'),
  controller: SignupModalController,
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  }
};
