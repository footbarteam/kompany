/**
 * Modal handling game creation.
 */
class GameCreateModalController {
  /** @ngInject */
  constructor($log, alertService, moment, fabinhoAPIService, $translate, $q) {
    // Bind useful services.
    this.log = $log.log;
    this.moment = moment;
    this.fabinho = fabinhoAPIService;
    this.$q = $q;
    this.alert = alertService;
    // Remove any alerts that could have been created by another component.
    this.alert.close();

    $translate(['GENERAL.REMOTE_ERROR', 'GAME.TYPES.TR', 'GAME.TYPES.ST', 'GAME.TYPES.FV', 'GAME.TYPES.11'])
      .then(translations => {
        this.translations = translations;

        this.gameTypes = [
          {key: 'tr', value: this.translations['GAME.TYPES.TR']},
          {key: 'st', value: this.translations['GAME.TYPES.ST']},
          {key: '5v', value: this.translations['GAME.TYPES.FV']},
          {key: '11', value: this.translations['GAME.TYPES.11']}
        ];
      });
  }

  /**
   * Prepare game forms when ready.
   */
  $onInit() {
    this.getGames()
      .then(games => {
        // Sort games here instead of dynamically to avoid change on live update.
        this.existingGames = games.sort((a, b) => this.moment(a.start_date) - this.moment(b.start_date));
        // If there are game, set for update, otherwise set new game.
        if (this.existingGames.length > 0) {
          return this.selectGame(0);
        }

        // Add empty game.
        this.addNewGame();
      })
      .then(() => this.getTrainees())
      .then(trainees => {
        this.trainees = trainees;
      });
  }

  /**
   * Set specific game as selected.
   */
  selectGame(index) {
    this.selectedGame = index;
    this.game = this.existingGames[this.selectedGame];

    return this.fabinho.getSessionAttributionList({game: this.game.id})
      .then(sessions => {
        this.game.sessions = sessions.results;
        // Also set good start and stop time.
        this.formStartTime = this.moment(this.game.start_date).toDate();
        this.formStopTime = this.moment(this.game.stop_date).toDate();
      });
  }

  /**
   * Add a new game to edit.
   */
  addNewGame() {
    this.game = {
      training_group: this.resolve.trainingGroup.id, // eslint-disable-line camelcase
      game_type: 'tr', // eslint-disable-line camelcase
      sessions: []
    };
    // Also set good start and stop time.
    this.formStartTime = this.moment().startOf('minute').toDate();
    this.formStopTime = this.moment().startOf('minute').add(1, 'hours').toDate();
  }

  /**
   * Get the list of existing game for the day and the training group.
   */
  getGames() {
    return this.fabinho.getGameAssignmentList({
      start_date_utc_0: this.resolve.day.format(),  // eslint-disable-line camelcase
      start_date_utc_1: this.resolve.day.clone().endOf('date').format(),  // eslint-disable-line camelcase
      training_group: this.resolve.trainingGroup.id  // eslint-disable-line camelcase
    });
  }

  /**
   * Fetch all the registered trainee for the day.
   */
  getTrainees() {
    return this.fabinho.getTraineeList({date: this.resolve.day.format()});
  }

  /**
   * Attach a session form to the existing game form.
   */
  addSessionToGame() {
    this.game.sessions.push({
      was_home: null  // eslint-disable-line camelcase
    });
  }

  /**
   * Filter only trainee that are not already in the lineup, so can be used.
   */
  get filteredTrainees() {
    return this.trainees.filter(
      trainee => !this.game.sessions.find(exSession => exSession.player === trainee.id)
    );
  }

  /**
   * Attach a session for each trainee of the training session
   */
  addRegularSessionsToGame() {
    // Filter players that belong to the good training session, and are not already here.
    const sessions = this.filteredTrainees.filter(
      trainee => trainee.training_group === this.resolve.trainingGroup.id
    ).map(trainee => {
      return {
        name: trainee.name,
        tracker: trainee.favorite_tracker_commercial_name,
        player: trainee.id,
        was_home: null  // eslint-disable-line camelcase
      };
    });

    this.game.sessions.push(...sessions);
  }

  /**
   * Save modification and close modal in case of success.
   */
  save() {
    // Validate good order.
    if (this.formStopTime < this.formStartTime) {
      this.alert.add('danger', 'The end date must come after the start date.');
      return;
    }

    // Populate dates.
    this.game.start_date = this.resolve.day.clone().set({  // eslint-disable-line camelcase
      hour: this.formStartTime.getHours(),
      minute: this.formStartTime.getMinutes()
    });
    this.game.stop_date = this.resolve.day.clone().set({  // eslint-disable-line camelcase
      hour: this.formStopTime.getHours(),
      minute: this.formStopTime.getMinutes()
    });

    this.gameSave()
      .then(updatedGame => {
        Object.assign(this.game, updatedGame);
        return this.$q.all(this.game.sessions.map(session => this.sessionSave(session)));
      })
      .then(() => this.close())
      .catch(error => {
        // Release submission flag.
        this.gameForm.$submitted = false;

        this.log(error);
        this.alert.add('danger', this.translations['GENERAL.REMOTE_ERROR']);
      });
  }

  /**
   * Perform saving or updating of game.
   */
  gameSave() {
    if (this.game.id) {
      return this.fabinho.updateGame(this.game.id, this.game);
    }
    return this.fabinho.createGame(this.game);
  }

  /**
   * Perform saving or updating of session.
   */
  sessionSave(session) {
    // Populate game key. Only here because before it may not exist.
    session.game = this.game.id;  // eslint-disable-line camelcase

    if (session.id) {
      return this.fabinho.updateSession(session.id, session);
    }
    return this.fabinho.createSession(session);
  }

  /**
   * Shortcut to properly display a game type.
   */
  gameTypesDisplay(type) {
    return this.gameTypes.find(elem => elem.key === type).value;
  }

  /**
   * Shortcut to properly display the name of a trainee.
   */
  getTraineeName(id) {
    return this.trainees.find(elem => elem.id === id).name;
  }

  /**
   * Temporary hack to display a tracker for each trainee. Must fetch real value on cantona.
   */
  getTraineeTracker(id) {
    return this.trainees.find(elem => elem.id === id).favorite_tracker_commercial_name;
  }

  /**
   * Set good attributes to object when selected from typeahead.
   */
  selectedPlayer(session, item) {
    session.name = item.name;
    session.player = item.id;
    session.tracker = item.favorite_tracker_commercial_name;
  }
}

export const GameCreateModal = {
  template: require('./GameCreateModal.html'),
  controller: GameCreateModalController,
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  }
};
