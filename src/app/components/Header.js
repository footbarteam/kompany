class HeaderController {
  /** @ngInject **/
  constructor(footbarAuthService) {
    this.auth = footbarAuthService;
  }
}

export const Header = {
  template: require('./Header.html'),
  controller: HeaderController
};
