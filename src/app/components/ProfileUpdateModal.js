import countries from 'i18n-iso-countries';

/**
 * Modal handling user profileUpdate.
 */
class ProfileUpdateModalController {
  /** @ngInject */
  constructor($log, okochaAPIService, alertService, $translate, footbarAuthService, $q) {
    this.auth = footbarAuthService;
    this.alert = alertService;
    // Remove any alerts that could have been created by another component.
    this.alert.close();
    this.log = $log.log;
    this.okocha = okochaAPIService;
    this.$q = $q;

    // Fetch strings.
    $translate(['GENERAL.REMOTE_ERROR'])
      .then(translations => {
        this.translations = translations;
      });
  }

  /**
   * Update the player, close the modal if ok.
   */
  profileUpdate() {
    // this.errors should be cleaned any time we make a new request.
    this.errors = [];

    // Profile picture is not mandatorily updated.
    const pictureUpdatedPromise = () => {
      if (this.auth.user.newPic) {
        return this.okocha.uploadProfilePic(this.auth.user.newPic);
      }
      return this.$q.when();
    };

    return pictureUpdatedPromise()
      .then(() => this.okocha.updateUser(this.parseData(this.auth.user)))
      .then(response => {
        this.auth.userUpdated(response);
        return this.close();
      })
      .catch(error => {
        // Deal with case of error not explicited.
        if (!error.data) {
          this.alert.add('danger', this.translations['GENERAL.REMOTE_ERROR']);
          this.log(error);
          return;
        }

        // Store errors to display in form.
        this.errors = error.data;

        // Add non field errors to top of page.
        if (angular.isDefined(this.errors.non_field_errors)) {
          this.errors.non_field_errors.map(nfError => this.alert.add('danger', nfError));
        }
      });
  }

  /**
   * Adapt data for good call to api.
   */
  parseData(user) {
    const data = Object.assign({}, user);
    delete data.profile_pic;
    return data;
  }

  getAvailableCountries(lang) {
    return countries.getNames(lang);
  }
}

export const ProfileUpdateModal = {
  template: require('./ProfileUpdateModal.html'),
  controller: ProfileUpdateModalController,
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  }
};
