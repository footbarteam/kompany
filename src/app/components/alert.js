/**
 * Service enabled to display an alert message.
 */
export class AlertService {
  /**
   * Add an alert, will be called by the component who needs
   * to display a message.
   */
  add(type, msg) {
    this.type = type;
    this.msg = msg;
  }

  /**
   * Remove the alert displayed, called when the user click on the
   * close button on the alert.
   */
  close() {
    this.type = null;
    this.msg = null;
  }
}
