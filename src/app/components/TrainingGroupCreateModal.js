/**
 * Modal handling traininggroup creation.
 */
class TrainingGroupCreateModalController {
  /** @ngInject */
  constructor($log, alertService, moment, fabinhoAPIService, $translate, $q) {
    // Bind useful services.
    this.log = $log.log;
    this.moment = moment;
    this.fabinho = fabinhoAPIService;
    this.$q = $q;
    this.alert = alertService;
    // Remove any alerts that could have been created by another component.
    this.alert.close();

    $translate(['GENERAL.REMOTE_ERROR'])
      .then(translations => {
        this.translations = translations;
      });
  }

  /**
   * Prepare traininggroup forms when ready.
   */
  $onInit() {
    this.trainingGroup = this.resolve.trainingGroup;

    if (this.trainingGroup) {
      // Get the trainees.
      this.fabinho.getTraineeList({training_group: this.trainingGroup.id})  // eslint-disable-line camelcase
        .then(players => {
          // Sort players here instead of dynamically to avoid change on live update.
          this.trainingGroup.players = players.sort(
            (a, b) => a.favorite_tracker_commercial_name.localeCompare(b.favorite_tracker_commercial_name)
          );
        });
    } else {
      this.trainingGroup = {
        players: []
      };
    }
  }

  /**
   * Attach a player form to the existing traininggroup form.
   */
  addPlayer() {
    this.trainingGroup.players.push({});
  }

  /**
   * Perform extra validation, save modification and close modal in case of success.
   */
  save() {
    // Populate dates.
    this.trainingGroup.start_date = this.moment(this.formStartDate).startOf('date');  // eslint-disable-line camelcase
    this.trainingGroup.stop_date = this.moment(this.formStopDate).endOf('date');  // eslint-disable-line camelcase

    // Validate good order.
    if (this.trainingGroup.stop_date < this.trainingGroup.start_date) {
      this.alert.add('danger', 'The end date must come after the start date.');
      return;
    }

    return this.trainingGroupSave()
      .then(updatedTrainingGroup => {
        Object.assign(this.trainingGroup, updatedTrainingGroup);
        return this.$q.all(this.trainingGroup.players.map(player => this.traineeSave(player)));
      })
      .then(() => this.close({$value: this.trainingGroup}))
      .catch(error => {
        // Release submission flag.
        this.tgForm.$submitted = false;

        this.log(error);
        this.alert.add('danger', this.translations['GENERAL.REMOTE_ERROR']);
      });
  }

  /**
   * Perform saving or updating of training group.
   */
  trainingGroupSave() {
    if (this.trainingGroup.id) {
      return this.fabinho.updateTrainingGroup(this.trainingGroup.id, this.trainingGroup);
    }
    return this.fabinho.createTrainingGroup(this.trainingGroup);
  }

  /**
   * Perform saving or updating of trainee.
   */
  traineeSave(trainee) {
    // Populate training group key. Only here because before it may not exist.
    trainee.training_group = this.trainingGroup.id;  // eslint-disable-line camelcase

    if (trainee.id) {
      return this.fabinho.updateTrainee(trainee.id, trainee);
    }
    return this.fabinho.createTrainee(trainee);
  }
}

export const TrainingGroupCreateModal = {
  template: require('./TrainingGroupCreateModal.html'),
  controller: TrainingGroupCreateModalController,
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  }
};
