import algoliasearch from 'algoliasearch';

/**
 * Modal handling user profileMerge.
 */
class ProfileMergeModalController {
  /** @ngInject */
  constructor($log, okochaAPIService, alertService, $translate, footbarAuthService) {
    this.auth = footbarAuthService;
    this.alert = alertService;
    // Remove any alerts that could have been created by another component.
    this.alert.close();
    this.log = $log.log;
    this.okocha = okochaAPIService;

    // Fetch strings.
    $translate(['GENERAL.REMOTE_ERROR'])
      .then(translations => {
        this.translations = translations;
      });

    // Prepare Algolia search.
    this.searchIndex = algoliasearch(
      process.env.ALGOLIA_APP, process.env.ALGOLIA_API_KEY
    ).initIndex(process.env.ALGOLIA_INDEX);

    // Default for selected profiles.
    this.selectedProfiles = [];
  }

  /**
   * Merge the player, close the modal if ok.
   */
  profileMerge() {
    // Builds the list of siblings.
    const siblings = this.selectedProfiles.map(profile => profile.id);

    return this.okocha.createMergeRequest({siblings})
      .then(() => this.close())
      .catch(error => {
        this.log(error);
        this.alert.add('danger', this.translations['GENERAL.REMOTE_ERROR']);
      });
  }

  /**
   * Cancel the merging of profile. User won't be prompted anymore.
   */
  profileMergeCancel() {
    return this.okocha.updateUser({needs_merge: false})  // eslint-disable-line camelcase
      .then(() => this.close())
      .catch(error => {
        this.log(error);
        this.alert.add('danger', this.translations['GENERAL.REMOTE_ERROR']);
      });
  }

  /**
   * Called by tags input directive, should return the matching players.
   */
  loadAutocompletion(query) {
    return this.searchIndex.search(query)
      .then(result => {
        return result.hits.map(profile => {
          return {text: profile.name, id: profile.objectID};
        });
      });
  }
}

export const ProfileMergeModal = {
  template: require('./ProfileMergeModal.html'),
  controller: ProfileMergeModalController,
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  }
};
