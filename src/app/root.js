import numeral from 'numeral';

/**
 * Root element of navigation. Resolve stuffs common to all pages.
 * For example, here is the language retrieving policy:
 * If url is preceeded by /fr, content must be returned in french.
 * If no language code, let translate module negiocated the lang with user, french or default english.
 */
class RootController {
  /** @ngInject **/
  constructor(
    $translate, $log, $state, $stateParams, $window, $transitions, $cookies, $rootScope,
    $http, amMoment, alertService
  ) {
    // Attach useful services.
    this.translate = $translate;
    this.moment = amMoment;
    this.alert = alertService;
    this.rootScope = $rootScope;
    this.http = $http;
    this.stateParams = $stateParams;

    // Deal with language retrieving.
    if (angular.isUndefined($cookies.get('lang'))) {
      // No language was set, trust $translate negotatiation if english, or force to french.
      $cookies.put('lang', ($stateParams.lang === 'en') ? ($translate.use() || 'en') : 'fr');
    }
    // Language cookie is defined, so use it.
    $stateParams.lang = $cookies.get('lang');
    this.setLocale($stateParams.lang);
    $state.go('.', $stateParams, {notify: false});

    // Listen to state change, in case language is changed.
    $transitions.onSuccess({}, () => {
      if ($cookies.get('lang') !== $stateParams.lang) {
        $cookies.put('lang', $stateParams.lang);
        this.setLocale($stateParams.lang);
      }
    });
  }

  /**
   * As switching a locale may require multiple operation, create a dedicated method.
   */
  setLocale(locale) {
    this.translate.use(locale);
    numeral.locale(locale); // FIXME: on dynamic change, numeral filter are not reevaluated.
    this.moment.changeLocale(locale);
    // In any case, set it in root scope to easily use in templates.
    this.rootScope.lang = this.stateParams.lang;
    // Store also in http header to get localized requests.
    this.http.defaults.headers.common["Accept-Language"] = this.stateParams.lang;
  }
}

export const rootComponent = {
  template: require('./root.html'),
  controller: RootController
};
