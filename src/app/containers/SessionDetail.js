class SessionDetailController {
  /** @ngInject */
  constructor($log, okochaAPIService, fabinhoAPIService, $stateParams) {
    // Bind useful services.
    this.okochaAPI = okochaAPIService;
    this.fabinhoAPI = fabinhoAPIService;
    this.log = $log.log;

    this.fabinhoAPI.getSessionDetail({id: $stateParams.id})
      .then(session => {
        this.session = session;
        // In case of owner not existing, try to populate it.
        if (!this.session.player.owner) {
          return this.fabinhoAPI.populateTrainee(this.session.player.id);
        }
        return this.session.player;
      })
      .then(trainee => {
        return this.fabinhoAPI.getPlayerDetail({okocha_id: trainee.owner});  // eslint-disable-line camelcase
      })
      .then(player => {
        this.player = player;
        return this.okochaAPI.getProfileDetail({id: this.player.okocha_id});
      })
      .then(profile => {
        this.profile = profile;
      });
  }
}

export const SessionDetail = {
  template: require('./SessionDetail.html'),
  controller: SessionDetailController
};
