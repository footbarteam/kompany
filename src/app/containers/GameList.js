/**
 * Controller for the games list view.
 */
class GameListController {
  /** @ngInject */
  constructor($log, $q, fabinhoAPIService, alertService, moment, $state) {
    this.$q = $q;
    this.fabinhoAPI = fabinhoAPIService;
    this.log = $log.log;
    this.alertService = alertService;
    this.moment = moment;
    this.$state = $state;

    this.pageIndex = 1;
    this.games = [];

    this.gameTypes = [
      {key: 'tr', value: 'GAME.TYPES.TR'},
      {key: 'st', value: 'GAME.TYPES.ST'},
      {key: '5v', value: 'GAME.TYPES.FV'},
      {key: '11', value: 'GAME.TYPES.11'}
    ];

    this.getNextPage();
  }

  /**
   * Get the next page of games.
   */
  getNextPage() {
    // Check if already fetching data or if last page.
    if (this.busy === true) {
      return;
    }
    // Prevent from double fetching.
    this.busy = true;
    this.fabinhoAPI.getGameList({page: this.pageIndex})
      .then(result => {
        this.setGames(result);
      });
  }

  /**
   * Add the fetched games to the list of populated games.
   */
  setGames(gameData) {
    return this.$q(() => {
      // Concatenate the lists, and keep only game finished already.
      this.games = [...this.games, ...gameData.results.filter(game => this.moment().isAfter(game.stop_date) && (game.game_type !== 'st'))];
      if (gameData.next !== null) {
        this.busy = false;
        this.pageIndex++;
      }
    });
  }

  /**
   * Shortcut to properly display a game type.
   */
  gameTypesDisplay(type) {
    return this.gameTypes.find(elem => elem.key === type).value;
  }

  /**
   * The url of the game depend of it's type.
   */
  gameDetailLink(game) {
    return this.$state.href((game.game_type === 'st') ? 'skillTests' : 'gameDetail', {id: game.id});
  }
}

export const GameList = {
  template: require('./GameList.html'),
  controller: GameListController
};
