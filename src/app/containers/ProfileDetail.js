export class ProfileDetailController {
  /** @ngInject */
  constructor(okochaAPIService, fabinhoAPIService, $log, alertService, $translate, $stateParams, footbarAuthService, $scope) {
    // Bind useful services.
    this.okochaAPI = okochaAPIService;
    this.fabinhoAPI = fabinhoAPIService;
    this.log = $log.log;
    this.alert = alertService;
    this.auth = footbarAuthService;

    $translate(['GENERAL.NOT_LOGGED_ERROR'])
      .then(translations => {
        this.translations = translations;
      })
      .then(() => this.auth.userPromise)  // Wait for user to be loaded.
      .then(() => this.getProfile($stateParams))
      .then(profile => {
        this.profile = profile;
        return this.fabinhoAPI.getPlayerDetail({okocha_id: this.profile.id});  // eslint-disable-line camelcase
      })
      .then(player => {
        this.player = player;
        return this.fabinhoAPI.getSessionList({player: this.player.okocha_id});
      })
      .then(response => {
        this.sessions = response.results.filter(session => session.score > 10);
      });

    // In case of user updated, update the profile.
    $scope.$on('userUpdated', () => {
      this.getProfile($stateParams)
        .then(profile => {
          this.profile = profile;
        });
    });
  }

  /**
   * Method to get the profile. Can be overridden if the logic changes.
   */
  getProfile(params) {
    return this.okochaAPI.getProfileDetail({id: params.id});
  }

  /**
   * Returns true if the user is logged as himself.
   */
  get isMe() {
    return (this.auth.user && this.profile) ? (this.profile.user == this.auth.user.id) : false;  // eslint-disable-line eqeqeq
  }
}

export const ProfileDetail = {
  template: require('./ProfileDetail.html'),
  controller: ProfileDetailController
};
