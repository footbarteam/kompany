class WorldcupController {
  /** @ngInject */
  constructor() {
    // Build the clubs ranking.
    this.clubs = [
      {
        name: 'Manchester City FC',
        score: 458,
        logo: 'manchester_city_fc.png'
      },
      {
        name: 'New York City FC',
        score: 449,
        logo: 'new_york_city_fc.png'
      },
      {
        name: 'Melbourne City FC',
        score: 412,
        logo: 'melbourne_city_fc.png'
      },
      {
        name: 'Yokohama F Marinos',
        score: 402,
        logo: 'yokohama_marinos.png'
      },
      {
        name: 'Atletico Torque',
        score: 386,
        logo: 'atletico_torque.png'
      },
      {
        name: 'Girona FC',
        score: 312,
        logo: 'girona_fc.png'
      },
      {
        name: 'Sichuan Jiuniu',
        score: 298,
        logo: 'sichuan_jiuniu.png'
      }
    ];
  }
}

export const Worldcup = {
  template: require('./Worldcup.html'),
  controller: WorldcupController
};
