import {ProfileDetailController} from './ProfileDetail';

/**
 * Behave like ProfileDetailController but precise that the needed profile is the user's.
 */
class MyProfileDetailController extends ProfileDetailController {
  /** @ngInject */
  constructor(okochaAPIService, fabinhoAPIService, $log, alertService, $translate, $stateParams, footbarAuthService, $scope, $q, $state) {
    super(okochaAPIService, fabinhoAPIService, $log, alertService, $translate, $stateParams, footbarAuthService, $scope);

    // Bind useful services.
    this.$q = $q;

    // In case of disconnexion, force reload.
    $scope.$on('userDisconnected', () => {
      $state.reload();
    });
  }

  /**
   * Overridden method to get the profile corresponding to the logged in user.
   */
  getProfile() {
    return this.$q(resolve => {
      // First rely on logged user.
      if (this.auth.user) {
        resolve();
      } else {
        // If user can't be retrieved, let's propose to login.
        this.auth.openModal('loginModal', 'static').then(() => resolve());
      }
    })
      .then(() => this.okochaAPI.getProfileDetail({user_id: this.auth.user.id}));  // eslint-disable-line camelcase
  }
}

export const MyProfileDetail = {
  template: require('./ProfileDetail.html'),
  controller: MyProfileDetailController
};
