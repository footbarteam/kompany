class SkillTestsController {
  /** @ngInject */
  constructor($log, fabinhoAPIService, $stateParams) {
    // Bind useful services.
    this.log = $log.log;

    // Initialize data.
    this.activeColumn = 'yoyo';

    fabinhoAPIService.getSkillTestsList({game: $stateParams.id})
      .then(sessions => {
        this.log(sessions);
      });
  }
}

export const SkillTests = {
  template: require('./SkillTests.html'),
  controller: SkillTestsController
};
