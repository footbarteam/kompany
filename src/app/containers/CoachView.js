class CoachViewController {
  /** @ngInject */
  constructor(fabinhoAPIService, moment, $log, $uibModal) {
    // Bind useful service.
    this.moment = moment;
    this.log = $log.log;
    this.uibModal = $uibModal;

    fabinhoAPIService.getTrainingGroupList()
      .then(trainingGroups => {
        this.trainingGroups = trainingGroups;
        this.trainingGroups.forEach(tg => {
          tg.dates = this.buildDateRange(tg.start_date, tg.stop_date);
        });
      })
      .then(() => {
        fabinhoAPIService.getTraineeList({date: this.moment().format()})
          .then(trainees => {
            this.fullTrainees = trainees;
          });
      });
  }

  /**
   * Build a range of date included in two dates.
   */
  buildDateRange(start, stop) {
    const dates = [];
    while (this.moment(stop).diff(start, 'days', true) > 0) {
      dates.push(this.moment(start).startOf('date'));
      start = this.moment(start).add(1, 'days');
    }
    return dates;
  }

  /**
   * Open a modal to manage game creation.
   */
  openGameCreateModal(trainingDay, trainingGroup) {
    return this.uibModal.open({
      animation: true,
      backdrop: 'static',
      component: 'gameCreateModal',
      resolve: {
        day: trainingDay,
        trainingGroup
      }
    }).result;
  }

  /**
   * Open a modal to manage traininggroup creation.
   */
  openTrainingGroupCreateModal(trainingGroup) {
    return this.uibModal.open({
      animation: true,
      backdrop: 'static',
      component: 'trainingGroupCreateModal',
      resolve: {
        trainingGroup
      }
    }).result
      .then(updatedTrainingGroup => {
        if (!trainingGroup) {
          trainingGroup = updatedTrainingGroup;
          this.trainingGroups.push(trainingGroup);
        }
        // Update dates.
        trainingGroup.dates = this.buildDateRange(trainingGroup.start_date, trainingGroup.stop_date);
      });
  }
}

export const CoachView = {
  template: require('./CoachView.html'),
  controller: CoachViewController
};
