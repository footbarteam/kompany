const positionMap = {
  0: 'CB',
  1: 'ST',
  2: 'CB',
  3: 'CM',
  4: 'AT',
  5: 'WB',
  6: 'CB',
  7: 'CAM',
  8: 'ST',
  9: 'CM',
  10: 'CAM',
  11: 'CAM',
  12: 'CB',
  13: 'AT',
  14: 'ST',
  15: 'WB'
};

const positionLimits = {
  '5v': {
    CB: 1,
    CM: 1,
    AT: 1
  },
  '11': {
    CB: 3,
    CM: 3,
    AT: 2
  }
};

/**
 * Controller to manage the game detail.
 */
class GameDetailController {
  /**
   * Constructor fetch data:
   * - Game details.
   * - List of sessions details.
   *
   * @param stateParams       - Retrieve the url parameters.
   * @param $log              - Logging service.
   * @param alertService      - Allow to display an error.
   * @param cantonaAPIService - Allow to fetch data from Cantona server.
   * @param fabinhoAPIService - Allow to fetch data from Fabinho server.
   */
  /** @ngInject */
  constructor($stateParams, $log, alertService, cantonaAPIService, fabinhoAPIService, $q) {
    // Initialize services.
    this.log = $log.log;
    this.alertService = alertService;
    this.cantonaAPI = cantonaAPIService;
    this.fabinhoAPI = fabinhoAPIService;
    this.$q = $q;

    // Get data from url parameters.
    const gameId = $stateParams.id;

    // Set active column.
    this.activeColumn = 'score';

    this.gameTypes = [
      {key: 'tr', value: 'GAME.TYPES.TR'},
      {key: 'st', value: 'GAME.TYPES.ST'},
      {key: '5v', value: 'GAME.TYPES.FV'},
      {key: '11', value: 'GAME.TYPES.11'}
    ];

    this.getGame(gameId)
      .then(() => this.getSessions())
      .then(() => {
        // Last step is to remove sessions that have no local id.
        this.sessions = this.sessions.filter(session => session.localId);
        return this.buildLineUp();
      });
  }

  /**
   * Promise to get the details data about game with specified id.
   *
   * @param gameId - Id of the game to fetch.
   */
  getGame(gameId) {
    return this.fabinhoAPI.getGameDetail({id: gameId})
      .then(game => {
        this.game = game;
        // Show Lineup.
        this.showLineUp = (this.game.game_type === '11' || this.game.game_type === '5v');
      });
  }

  /**
   * Promise to get the details data about the list of session
   * belonging to the current game.
   */
  getSessions() {
    /** Get the data from Cantona and populate sessions if success. */
    return this.cantonaAPI.getSessionList({game: this.game.storing_id})
      .then(sessions => {
        this.sessions = sessions.results.filter(session => session.distance_run > 500);
        return this.$q.all(this.sessions.map(session => {
          return this.fabinhoAPI.getSessionDetail({storing_id: session.id})  // eslint-disable-line camelcase
            .then(localSession => {
              if (!localSession.id) {
                throw new Error(`No such report ${session.id} on fabinho.`);
              }
              // Check the compared score to see if it's worth refetching.
              if (Math.round(session.score) !== Math.round(localSession.score)) {
                return this.fabinhoAPI.populateSession(localSession.id);
              }
              return localSession;
            })
            .then(localSession => {
              session.localId = localSession.id;
              session.position = this.getPosition(localSession.playing_style);
              session.wasHome = localSession.was_home;
              session.name = localSession.player.name;
              session.id = localSession.player.id;
            })
            .catch(error => {
              this.log(error);
            });
        }));
      });
  }

  /**
   * Find out a position from the playing style.
   */
  getPosition(style) {
    return positionMap[style];
  }

  /**
   * According to each player's position, build a nice lineup.
   */
  spreadPlayers(sessions) {
    const skeleton = {
      WB: [],
      CB: [],
      CM: [],
      WM: [],
      AT: [],
      WF: []
    };

    const positionSpreader = (lineUp, session, index, allPlayers) => {
      switch (session.position) {
        case 'WB':
          if (allPlayers.filter(s => s.position === 'WB').length > 1 && lineUp.WB.length < 2) {
            // In case we have at least two lat defenders and they are not populated, put him.
            lineUp.WB.push(session);
          } else if (allPlayers.filter(s => s.position === 'CAM').length > 0 && lineUp.WM.length < 2) {
            // Otherwise, if we have an offensive midfielder, they can both go on lat mid.
            lineUp.WM.push(session);
          } else if (lineUp.CB.length < positionLimits[this.game.game_type].CB) {
            // Otherwise put him center back while we don't have too many.
            lineUp.CB.push(session);
          }
          break;

        case 'CB':
          if (lineUp.CB.length < positionLimits[this.game.game_type].CB) {
            lineUp.CB.push(session);
          }
          break;

        case 'CM':
          if (lineUp.CM.length < positionLimits[this.game.game_type].CM) {
            lineUp.CM.push(session);
          }
          break;

        case 'CAM':
          if (lineUp.WM.length < 2 && (
            allPlayers.filter(s => s.position === 'CAM').length > 1 ||
              allPlayers.filter(s => s.position === 'WB').length > 2
          )) {
            // In case we have at least two offensive mid or more than 3 wb put them on the side.
            lineUp.WM.push(session);
          } else if (lineUp.CM.length < positionLimits[this.game.game_type].CM) {
            // Otherwise put him center mid while we don't have too many.
            lineUp.CM.push(session);
          } else if (lineUp.AT.length < 2) {
            // Eventually can play as a center forward.
            lineUp.AT.push(session);
          }
          break;

        case 'AT':
          if (allPlayers.filter(s => s.position === 'AT').length > 1 && lineUp.WF.length < 2) {
            // In case we have at least two attackers, put them on the wings.
            lineUp.WF.push(session);
          } else if (lineUp.AT.length < positionLimits[this.game.game_type].AT) {
            // Otherwise put him center mid while we don't have too many.
            lineUp.AT.push(session);
          }
          break;

        case 'ST':
          if (lineUp.AT.length < positionLimits[this.game.game_type].AT) {
            lineUp.AT.push(session);
          }
          break;

        default:
      }
      return lineUp;
    };

    const futureLineUp = sessions.reduce(positionSpreader, skeleton);

    // Eventually squeeze results in three lines.
    return [
      [...futureLineUp.WB.slice(0, 1), ...futureLineUp.CB, ...futureLineUp.WB.slice(1, 2)],
      [...futureLineUp.WM.slice(0, 1), ...futureLineUp.CM, ...futureLineUp.WM.slice(1, 2)],
      [...futureLineUp.WF.slice(0, 1), ...futureLineUp.AT, ...futureLineUp.WF.slice(1, 2)]
    ];
  }

  /**
   * Split players into a lineup.
   */
  buildLineUp() {
    const homePlayers = this.sessions.filter(s => s.wasHome === true).sort((a, b) => b.score - a.score);
    const awayPlayers = this.sessions.filter(s => s.wasHome === false).sort((a, b) => b.score - a.score);
    if (homePlayers.length === 0 && homePlayers.length === 0) {
      this.showLineUp = false;
      return;
    }

    this.players = [this.spreadPlayers(homePlayers), this.spreadPlayers(awayPlayers)];
  }

  /**
   * Shortcut to properly display a game type.
   */
  gameTypesDisplay(type) {
    return this.gameTypes.find(elem => elem.key === type).value;
  }
}

export const GameDetail = {
  template: require('./GameDetail.html'),
  controller: GameDetailController
};
