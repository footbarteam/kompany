class LeaderboardController {
  /** @ngInject */
  constructor(okochaAPIService, fabinhoAPIService, $log) {
    // Bind useful services.
    this.okochaAPI = okochaAPIService;
    this.fabinhoAPI = fabinhoAPIService;
    this.log = $log.log;

    // Initialize data.
    this.leaderboard = [];
    this.scoreMap = {};
    this.pageIndex = 1;

    // Get the remote leaderboard.
    this.buildLineup();
    this.getNextPage();
  }

  /**
   * Get the next page of players.
   */
  getNextPage(params = {}) {
    // Check if already fetching data or if last page.
    if (this.busy === true) {
      return;
    }

    // Prevent from double fetching.
    this.busy = true;

    // Add relevant page.
    params.page = this.pageIndex;
    params.limit = 10;

    // Make request.
    return this.fabinhoAPI.getPlayerList(params)
      .then(result => {
        this.dataRemaining = (result.next !== null);
        return this.setProfiles(result);
      })
      .then(() => {
        if (this.dataRemaining) {
          this.busy = false;
          this.pageIndex++;
        }
      });
  }

  /**
   * Add the fetched profiles to the list of populated profiles.
   */
  setProfiles(profileData) {
    // Concatenate the lists.
    this.leaderboard = [...this.leaderboard, ...profileData.results];
  }

  /**
   * Build the starting eleven of equivalents.
   */
  buildLineup() {
    // Get the classic ManCity lineup.
    this.players = [
      [
        {
          equivalent: {
            name: 'benjamin mendy',
            fifaId: 204884
          }
        },
        {
          equivalent: {
            name: 'nicolas otamendi',
            fifaId: 192366
          }
        },
        {
          equivalent: {
            name: 'john stones',
            fifaId: 203574
          }
        },
        {
          equivalent: {
            name: 'danilo',
            fifaId: 199304
          }
        }
      ],
      [
        {
          equivalent: {
            name: 'leroy sane',
            fifaId: 222492
          }
        },
        {
          equivalent: {
            name: 'david silva',
            fifaId: 214378
          }
        },
        {
          equivalent: {
            name: 'fernandinho',
            fifaId: 135507
          }
        },
        {
          equivalent: {
            name: 'riyad mahrez',
            fifaId: 204485
          }
        }
      ],
      [
        {
          equivalent: {
            name: 'gabriel jesus',
            fifaId: 230666
          }
        },
        {
          equivalent: {
            name: 'bernardo silva',
            fifaId: 218667
          }
        }
      ]
    ];

    this.players.forEach(line => {
      line.forEach(player => {
        this.fabinhoAPI.getDoppelganger(player.equivalent.fifaId)
        // .then(fabinhoPlayer => {
        //   return this.okochaAPI.getExportedProfileDetail({id: fabinhoPlayer.okocha_id});
        // })
          .then(profile => {
            // Load attributes to player.
            Object.assign(player, {
              // profilePic: profile.profile_pic,
              name: profile.name,
              // id: profile.id
              id: profile.okocha_id
            });
          })
          .catch(() => {});
      });
    });
  }
}

export const Leaderboard = {
  template: require('./Leaderboard.html'),
  controller: LeaderboardController
};
