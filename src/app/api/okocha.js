// Url has to be built from conf.
export const okochaUrl = `${process.env.PROTOCOL}${process.env.OKOCHA_DOMAIN}/`;

/**
 * Service to fetch data from the okocha API.
 */
export class OkochaAPIService {
  /**
   * Initiliaze class by importing services.
   *
   * @param $resource - Library to make REST api calls.
   */
  /** @ngInject */
  constructor($resource, Upload, requestManager) {
    this.$resource = $resource;
    this.Upload = Upload;
    this.requestManager = requestManager;
  }

  /**
   * Get a list of profiles. Request is filtered and paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the profile list.
   */
  getProfileList(params) {
    return this.requestManager.getPaginatedData(`${okochaUrl}profile/list/`, params);
  }

  /**
   * Get a detailed profiles. Request is filtered and paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the profile list.
   */
  getProfileDetail(params) {
    return this.requestManager.getSingleData(`${okochaUrl}profile/detail/`, params);
  }

  /**
   * Get a detailed profiles but with only canonical info. Request is filtered and paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the profile list.
   */
  getExportedProfileDetail(params) {
    return this.requestManager.getSingleData(`${okochaUrl}profile/list/`, params);
  }

  /**
   * Get a list of sessions. Request is filtered and paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the session list.
   */
  getSessionList(params) {
    const sessionListResource = this.$resource(`${okochaUrl}session/list/`, params);
    return sessionListResource.get().$promise;
  }

  /**
   * Request a password reset.
   *
   * @param data   - Data to post including user email.
   *
   * @return promise - Promise to request a password reset.
   */
  requestPasswordReset(data) {
    const resetPasswordResource = this.$resource(`${okochaUrl}rest-auth/password/reset/`);
    return resetPasswordResource.save(data).$promise;
  }

  /**
   * Save a new password.
   *
   * @param data   - Data to post including user uid, token, new_password1 and new_password2.
   *
   * @return promise - Promise to save the new password.
   */
  saveNewPassword(data) {
    const saveNewPasswordResource = this.$resource(`${okochaUrl}rest-auth/password/reset/confirm/`);
    return saveNewPasswordResource.save(data).$promise;
  }

  /**
   * Get user info. This will respond 401 Unauthorized if user is not logged in.
   *
   * @return promise - Promise to get the user info.
   */
  getUser() {
    const getUserResource = this.$resource(`${okochaUrl}rest-auth/user/`);
    return getUserResource.get().$promise;
  }

  /**
   * Update user info. This will respond 401 Unauthorized if user is not logged in.
   *
   * @param data - Data to patch including user email, first_name and last_name.
   *
   * @return promise - Promise to update the user info.
   */
  updateUser(user) {
    const patchUserResource = this.$resource(`${okochaUrl}rest-auth/user/`, {}, {update: {method: 'PATCH'}});
    return patchUserResource.update(user).$promise;
  }

  /**
   * Upload profile picture to okocha.
   *
   * @param file - Profile picture to upload.
   */
  uploadProfilePic(file) {
    return this.Upload.upload({
      url: `${okochaUrl}rest-auth/user/`,
      data: {profile_pic: file}, // eslint-disable-line camelcase
      method: 'PATCH' // User is created before the possibility of adding a picture.
    });
  }

  /**
   * Update user password. This will respond 401 Unauthorized if user is not logged in.
   *
   * @param password - Password object containing old_password, new_password1, new_password2.
   *
   * @return promise - Promise to update the password.
   */
  updatePassword(password) {
    const updatePasswordResource = this.$resource(`${okochaUrl}rest-auth/password/change/`);
    return updatePasswordResource.save(password).$promise;
  }

  /**
  * Get some profiles that matches query.
  */
  queryProfiles(query) {
    const queryProfilesResource = this.$resource(`${okochaUrl}mergeable-profile/list`, query);
    return queryProfilesResource.get().$promise;
  }

  /**
  * Create a merge request for a profile list.
  */
  createMergeRequest(siblings) {
    const mergeRequestResource = this.$resource(`${okochaUrl}profile/merge-request/`);
    return mergeRequestResource.save(siblings).$promise;
  }
}
