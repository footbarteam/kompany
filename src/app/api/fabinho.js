// Url has to be built from conf.
export const fabinhoUrl = `${process.env.PROTOCOL}${process.env.FABINHO_DOMAIN}/`;

/**
 * Service to fetch data from the fabinho API.
 */
export class FabinhoAPIService {
  /**
   * Initiliaze class by importing services.
   *
   * @param $resource - Library to make REST api calls.
   */
  /** @ngInject */
  constructor($resource, $log, requestManager) {
    this.$resource = $resource;
    this.log = $log.log;
    this.requestManager = requestManager;
  }

  /**
   * Get a list of players. Request is filtered and paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the player list.
   */
  getPlayerList(params) {
    return this.requestManager.getPaginatedData(`${fabinhoUrl}player/list/`, params);
  }

  /**
   * Get a player.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the player.
   */
  getPlayerDetail(params) {
    return this.requestManager.getSingleData(`${fabinhoUrl}player/list/`, params);
  }

  /**
   * Create a new player.
   *
   * @param okocha_id - Id of the profile object on okocha.
   *
   * @return promise  - Promise to get the new player object.
   */
  createPlayer(okochaId) {
    const playerResource = this.$resource(`${fabinhoUrl}player/create/`);
    return playerResource.save({okocha_id: okochaId}).$promise;  // eslint-disable-line camelcase
  }

  /**
   * Update a player's data by fetching new sessions.
   *
   * @param okocha_id - Id of the profile object on okocha.
   *
   * @return promise  - Promise without content.
   */
  updatePlayer(okochaId) {
    const playerResource = this.$resource(`${fabinhoUrl}player/update/:id/`, {id: okochaId});
    return playerResource.save().$promise;
  }

  /**
   * Promise to create a training group on backend.
   *
   * @params sessionData - Necessary data to create a training group.
   *
   * @return promise  - Promise to get the data corresponding to the created training group.
   */
  createSession(sessionData) {
    const sessionCreateResource = this.$resource(`${fabinhoUrl}session/create/`);
    return sessionCreateResource.save(sessionData).$promise;
  }

  /**
   * Promise to update a training group on backend.
   *
   * @params sessionId   - Id of the training group to update.
   * @params sessionData - Data to update the training group.
   *
   * @return promise  - Promise to get the data corresponding to the created training group.
   */
  updateSession(sessionId, sessionData) {
    const sessionUpdateResource = this.$resource(`${fabinhoUrl}session/update/${sessionId}/`);
    return sessionUpdateResource.save(sessionData).$promise;
  }

  /**
   * Promise to populate a session from cantona data.
   *
   * @params sessionId   - Id of the session to populate.
   *
   * @return promise  - Promise to get the new session data.
   */
  populateSession(sessionId) {
    const sessionUpdateResource = this.$resource(`${fabinhoUrl}session/populate/${sessionId}/`);
    return sessionUpdateResource.save().$promise;
  }

  /**
   * Get a list of sessions. Request is filtered and paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the session list.
   */
  getSessionList(params) {
    return this.requestManager.getPaginatedData(`${fabinhoUrl}session/list/`, params);
  }

  /**
   * Get a list of sessions, but serializer compatible with attribution mechanism.
   * Request is filtered and paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the session list.
   */
  getSessionAttributionList(params) {
    return this.requestManager.getPaginatedData(`${fabinhoUrl}session/attribution_list/`, params);
  }

  /**
   * Get a detailed session.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the session detail.
   */
  getSessionDetail(params) {
    return this.requestManager.getSingleData(`${fabinhoUrl}session/list/`, params);
  }

  /**
   * Get a list of skill test results. Request is filtered and not paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the session list.
   */
  getSkillTestsList(params) {
    return this.requestManager.getUnpaginatedData(`${fabinhoUrl}session/skill_tests/`, params);
  }

  /**
   * Get a the player corresponding to the fifa_id.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get a player object.
   */
  getDoppelganger(fifaId) {
    const playerResource = this.$resource(`${fabinhoUrl}doppelganger/:id/`, {id: fifaId});
    return playerResource.get().$promise;
  }

  /**
   * Promise to create a training group on backend.
   *
   * @params trainingGroupData - Necessary data to create a training group.
   *
   * @return promise  - Promise to get the data corresponding to the created training group.
   */
  createTrainingGroup(trainingGroupData) {
    const trainingGroupCreateResource = this.$resource(`${fabinhoUrl}training_group/create/`);
    return trainingGroupCreateResource.save(trainingGroupData).$promise;
  }

  /**
   * Promise to update a training group on backend.
   *
   * @params trainingGroupId   - Id of the training group to update.
   * @params trainingGroupData - Data to update the training group.
   *
   * @return promise  - Promise to get the data corresponding to the created training group.
   */
  updateTrainingGroup(trainingGroupId, trainingGroupData) {
    const trainingGroupUpdateResource = this.$resource(`${fabinhoUrl}training_group/update/${trainingGroupId}/`);
    return trainingGroupUpdateResource.save(trainingGroupData).$promise;
  }

  /**
   * Get a list of traininggroups. Request is filtered and paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the traininggroup list.
   */
  getTrainingGroupList(params) {
    return this.requestManager.getUnpaginatedData(`${fabinhoUrl}training_group/list/`, params);
  }

  /**
   * Promise to create a training group on backend.
   *
   * @params traineeData - Necessary data to create a training group.
   *
   * @return promise  - Promise to get the data corresponding to the created training group.
   */
  createTrainee(traineeData) {
    const traineeCreateResource = this.$resource(`${fabinhoUrl}trainee/create/`);
    return traineeCreateResource.save(traineeData).$promise;
  }

  /**
   * Promise to update a training group on backend.
   *
   * @params traineeId   - Id of the trainee to update.
   * @params traineeData - Data to update the trainee.
   *
   * @return promise  - Promise to get the data corresponding to the created training group.
   */
  updateTrainee(traineeId, traineeData) {
    const traineeUpdateResource = this.$resource(`${fabinhoUrl}trainee/update/${traineeId}/`);
    return traineeUpdateResource.save(traineeData).$promise;
  }

  /**
   * Promise to populate a trainee with its good owner from okocha.
   *
   * @params traineeId   - Id of the trainee to populate.
   *
   * @return promise  - Promise to get the new trainee data.
   */
  populateTrainee(traineeId) {
    const traineePopulateResource = this.$resource(`${fabinhoUrl}trainee/fetch-owner/${traineeId}/`);
    return traineePopulateResource.save().$promise;
  }

  /**
   * Get a list of trainees. Request is filtered and paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the trainee list.
   */
  getTraineeList(params) {
    return this.requestManager.getUnpaginatedData(`${fabinhoUrl}trainee/list/`, params);
  }

  /**
   * Promise to create a game on backend.
   *
   * @params gameData - Necessary data to create a game.
   *
   * @return promise  - Promise to get the data corresponding to the created game.
   */
  createGame(gameData) {
    const gameCreateResource = this.$resource(`${fabinhoUrl}game/create/`);
    return gameCreateResource.save(gameData).$promise;
  }

  /**
   * Promise to update a training group on backend.
   *
   * @params gameId   - Id of the training group to update.
   * @params gameData - Data to update the training group.
   *
   * @return promise  - Promise to get the data corresponding to the created training group.
   */
  updateGame(gameId, gameData) {
    const gameUpdateResource = this.$resource(`${fabinhoUrl}game/update/${gameId}/`);
    return gameUpdateResource.save(gameData).$promise;
  }

  /**
   * Get a list of games. Request is filtered and not paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the game list.
   */
  getGameAssignmentList(params) {
    return this.requestManager.getUnpaginatedData(`${fabinhoUrl}game/assignment/`, params);
  }

  /**
   * Get a list of games. Request is filtered and paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the game list.
   */
  getGameList(params) {
    return this.requestManager.getPaginatedData(`${fabinhoUrl}game/list/`, params);
  }

  /**
   * Get a detailed game.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the game list.
   */
  getGameDetail(params) {
    return this.requestManager.getSingleData(`${fabinhoUrl}game/list/`, params);
  }
}
