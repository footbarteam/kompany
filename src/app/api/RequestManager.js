/**
 *A manager to deal with http request.
 */
export class FootbarRequestManager {
  /**
   * Request manager. It can return:
   * - a detailed object
   * - an object containing the reslut of a paginated request (data, previous and next page)
   * - a list of objects for unpaginated views
   * In case of error it displays a general error alert.
   */
  /** @ngInject */
  constructor($resource, $translate, alertService, $log) {
    // Bind useful services.
    this.$resource = $resource;
    this.alert = alertService;
    this.log = $log.log;

    $translate(['GENERAL.REMOTE_ERROR'])
      .then(translations => {
        this.translations = translations;
      });
  }

  /**
   * Wrapper around angular resource common to all our requests.
   *
   * @param url          - the url to perform the request
   * @param filterParam  - the parameters to use to filter the request
   * @param getParam     - the parameters to alter the get method
   *
   * @return promise     - promise containing the required objects.
   */
  makeRequest(url, filterParam, getParam) {
    const dataResource = this.$resource(url, filterParam, {
      get: getParam
    });
    return dataResource.get().$promise
      .catch(error => {
        this.log(error);
        this.alert.add('danger', this.translations['GENERAL.REMOTE_ERROR']);
      });
  }

  /**
   * Returns a single object.
   *
   * @param url          - the url to perform the request
   * @param filterParam  - the parameters to use to filter the request
   *
   * @return promise - promise containing the required objects.
   */
  getSingleData(url, filterParam) {
    return this.makeRequest(url, filterParam, {
      // Specify a custom data deserializer.
      transformResponse: data => {
        const objectData = angular.fromJson(data).results[0];
        return objectData;
      }
    });
  }

  /**
   * Returns either a paginated response.
   *
   * @param url          - the url to perform the request
   * @param filterParam  - the parameters to use to filter the request
   *
   * @return promise - promise containing the required objects.
   */
  getPaginatedData(url, filterParam) {
    return this.makeRequest(url, filterParam, {
      // Specify a custom data deserializer.
      transformResponse: data => {
        const jsonData = angular.fromJson(data);
        return jsonData;
      }
    });
  }

  /**
   * Returns a promise containing a list of required objects.
   *
   * @param url          - the url to perform the request
   * @param filterParam  - the parameters to use to filter the request
   *
   * @return promise - promise containing the required objects.
   */
  getUnpaginatedData(url, filterParam) {
    return this.makeRequest(url, filterParam, {
      isArray: true,
      // Specify a custom data deserializer.
      transformResponse: data => {
        const jsonData = angular.fromJson(data);
        return jsonData;
      }
    });
  }
}
