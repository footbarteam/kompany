// Url has to be built from conf.
export const cantonaUrl = `${process.env.PROTOCOL}${process.env.CANTONA_DOMAIN}/`;

/**
 * Service to fetch data from the cantona API.
 */
export class CantonaAPIService {
  /**
   * Initiliaze class by importing services.
   *
   * @param $resource - Library to make REST api calls.
   */
  /** @ngInject */
  constructor($resource, $log, requestManager) {
    this.$resource = $resource;
    this.log = $log.log;
    this.requestManager = requestManager;
  }

  /**
   * Get a list of players. Request is filtered and paginated.
   *
   * @param params   - Request parameters.
   *
   * @return promise - Promise to get the player list.
   */
  getPlayerList(params) {
    params.event = 2;  // Quick and dirty filter not to be polluted by too many data.
    return this.requestManager.getPaginatedData(`${cantonaUrl}player/leaderboard/`, params);
  }

  /**
   * Promise to get the list of all games paginated.
   *
   * @param params   - Parameters to pass in the request (eg: filtering games by population, session,...).
   *
   * @return promise - Promise to get the game list paginated.
   */
  getGameList(params) {
    params.event = 2;  // Quick and dirty filter not to be polluted by too many data.
    return this.requestManager.getPaginatedData(`${cantonaUrl}game/list/`, params);
  }

  /**
   * Promise to get the game details from specified id.
   *
   * @param gameId   - Id of the game to fetch.
   *
   * @return promise - Promise to get a game object.
   */
  getGameDetail(params) {
    return this.requestManager.getSingleData(`${cantonaUrl}game/detail/`, params);
  }

  /**
   * Promise to get a list of detailled sessions.
   *
   * @param params   - parameter for the request ({game: gameId} or {id: sessionId}).
   *
   * @return promise - Promise to get a list of session objects.
   */
  getSessionList(params) {
    return this.requestManager.getPaginatedData(`${cantonaUrl}session/detail/`, params);
  }
}
