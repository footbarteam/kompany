# JS app to improve fan experience.

## Abstract

Simple web app based on angularjs created with [fountain generator](https://github.com/FountainJS/generator-fountain-webapp).
Will allow fans of a professionnal clubs to interact with their club anytime they play a game.

## Prerequistes

- Node 11.14+
- Npm 5.6+
- Gulp-cli

## Install

Make sure you have understood and successfully installed all the prerequisites.

    git clone git@bitbucket.org:footbarteam/kompany.git
    cd kompany
    npm install

## Serve

Launch a browser sync server on your source files:

    npm run serve

Launch a server on your optimized application:

    npm run serve:dist

## Test

Launch your unit tests with Karma:

    npm run test

Launch your unit tests with Karma in watch mode:

    npm run test:auto

## Deploying

    gulp deploy

## Appendix

### Contents

The building environment has been built with a yeoman generator, with the following components:

- Angular 1 as js framework.
- Gulp as building tool.
- ESlint as code linter.
- Karma as testing framework
- Webpack as module bundler.
- Less as css framewor.
- UI router.

### Convention

While coding, please follow:

- [ES2015](https://babeljs.io/learn-es2015/) also called ES6.
- [Angular components](https://docs.angularjs.org/guide/component).
- [JSDoc](http://usejsdoc.org/howto-es2015-classes.html).

### Documentation

Many stuffs has been written about javascript, so do not trust what you can google. The only documentation you need is:

- [Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)
- [Angular](https://docs.angularjs.org/guide)
- [UI Router](https://ui-router.github.io/ng1/docs/latest/)
- [UI Bootstrap](https://angular-ui.github.io)
- [Angular moment](https://github.com/urish/angular-moment) with [moment](http://momentjs.com/docs/)
- [Anguler translate](https://angular-translate.github.io/)
- The official doc of the different libraries installed.

### NPM usage

Npm install is a heavy task and sometimes some stuff may break. Do not hesitate to clean up and restart:

    rm -rf node_modules
    npm cache clean
    npm install

### Trailing slash policy

The use of a slash at the end of URL has always been discussed.
Our decision at Footbar is to follow Django's convention and always add a trailing slash at end of urls. If we try to access the url without trailing slash, the website will render a 301 redirecting to the trailing slash version.

Note that the url routing in this is performed by a third party module called ui-router. According to their documentation, we disable the strict mode, that allows both trailing slash or not urls (and no redirection is done).

### Webpack

A javascript project is made of a lot of frameworks and libraries, and it quickly becomes a mess to make it work and to bundle it together.

Hopefully there is a library that does all this work for us, it's called webpack, and the only thing we have to do is to configure it properly. To configure it you have to understand a minimum how this work, that's why I write this note.

Wepback is run during the gulp tasks like `gulp serve` or `gulp build`, according to the config file `webpack.conf.js` (respectively `webpack-dist.conf.js`).

The config file exports several values. `entry` is an array of input files, for us `src/index.js` that takes care of importing other sources. `output` is the location of the built files, for us `.tmp/index.js` (resp. `dist/index-hash.js`). `module.loaders` is a list of libraries that will transform our sources to a compiled file. There is also some `plugins`, that we shouldn't have to worry about.

A loader has basically two properties, `test` is a filter to specify on which file to act, and `use` (formerly `loader`) that is a list of functions to happen on those files.

If ever you have a problem with a gulp task, that seems related to webpack config (and it usually is as webpack handles most of the work), ask yourself where is your file included, and how it should be handled. Eventually the [doc](https://webpack.js.org) seems pretty clear, they even have a live chat if you have a doubt! However do not try to apply a configuration that you found randomly on the web, there are few chances that it works on your side, and even if it does, as you did not understand what it does, it will fail someday and you or your teammates will cry.

### First deployment procedure

Then build the files to deploy:
    gulp build

Copy some static files in dist/ folder.
- conf/app-nginx.conf.sigil
- conf/.static

Create a new git repository in dist/ folder and commit all the files. Then add the remote and push:

    git remote add dokku dokku@footbar.com:mancity
    git push dokku master

### Https support

Now thank's to the let's encrypt, having our domain signed is no longer expensive, it's just a question of adding the good [dokku plugin](https://github.com/dokku/dokku-letsencrypt) that does all the job. Particularly in this case, we can now sign footbar.com and all the aliases under the same certificate.
