const webpack = require('webpack');
const conf = require('./gulp.conf');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const FailPlugin = require('webpack-fail-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
  module: {
    loaders: [
      {
        test: /.json$/,
        loaders: [
          'json-loader'
        ]
      },
      {
        test: /.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        enforce: 'pre'
      },
      {
        test: /\.(css|less)$/,
        loaders: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          'less-loader'
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: [
          'ng-annotate-loader',
          'babel-loader'
        ]
      },
      {
        test: /.html$/,
        loaders: [
          'html-loader'
        ]
      },
      {
        test: /\.(png|jpg|woff|woff2|eot|ttf|svg|mp4|xml|gif)$/,
        loader: 'url-loader?limit=100000'
      }
    ]
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
    FailPlugin,
    new HtmlWebpackPlugin({
      template: conf.path.src('index.html')
    }),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: () => [autoprefixer]
      },
      debug: true
    }),
    // Set the dev backend url.
    new webpack.DefinePlugin({
      'process.env.PROTOCOL': JSON.stringify('http://'),
      'process.env.CANTONA_DOMAIN': JSON.stringify('127.0.0.1:8003'),
      'process.env.OKOCHA_DOMAIN': JSON.stringify('127.0.0.1:8004'),
      'process.env.FABINHO_DOMAIN': JSON.stringify('127.0.0.1:8005'),
      'process.env.ALGOLIA_INDEX': JSON.stringify('profile_index'),
      'process.env.ALGOLIA_APP': JSON.stringify('GOFWC3DH7J'),
      'process.env.ALGOLIA_API_KEY': JSON.stringify('afd18db1265ab086f3f03617b8cd6eba'),
      'process.env.FACEBOOK_APP': JSON.stringify('314867622452022')
    })
  ],
  devtool: 'source-map',
  output: {
    path: path.join(process.cwd(), conf.paths.tmp),
    filename: 'index.js'
  },
  entry: `./${conf.path.src('index')}`
};
