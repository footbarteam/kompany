const webpack = require('webpack');
const conf = require('./gulp.conf');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const FailPlugin = require('webpack-fail-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const pkg = require('../package.json');
const autoprefixer = require('autoprefixer');

module.exports = {
  module: {
    loaders: [
      {
        test: /.json$/,
        loaders: [
          'json-loader'
        ]
      },
      {
        test: /.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        enforce: 'pre'
      },
      {
        test: /\.(css|less)$/,
        loaders: ExtractTextPlugin.extract({
          fallbackLoader: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: 'minimize'
            },
            'postcss-loader',
            'less-loader'
          ]
        })
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: [
          'ng-annotate-loader',
          'babel-loader'
        ]
      },
      {
        test: /.html$/,
        loaders: [
          'html-loader'
        ]
      },
      {
        test: /\.(png|jpg|woff|woff2|eot|ttf|svg|mp4|xml|gif)$/,
        loader: 'url-loader?limit=100000'
      }
    ]
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
    FailPlugin,
    new HtmlWebpackPlugin({
      template: conf.path.src('index.html')
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {unused: true, dead_code: true, warnings: false} // eslint-disable-line camelcase
    }),
    new ExtractTextPlugin('index-[contenthash].css'),
    new webpack.optimize.CommonsChunkPlugin({name: 'vendor'}),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: () => [autoprefixer]
      }
    }),
    // Set the production backend url.
    new webpack.DefinePlugin({
      'process.env.PROTOCOL': JSON.stringify('https://'),
      'process.env.CANTONA_DOMAIN': JSON.stringify('playerbackend.footbar.com'),
      'process.env.OKOCHA_DOMAIN': JSON.stringify('okocha.footbar.com'),
      'process.env.FABINHO_DOMAIN': JSON.stringify('fabinho.footbar.com'),
      'process.env.ALGOLIA_INDEX': JSON.stringify('profile_index'),
      'process.env.ALGOLIA_APP': JSON.stringify('GOFWC3DH7J'),
      'process.env.ALGOLIA_API_KEY': JSON.stringify('afd18db1265ab086f3f03617b8cd6eba'),
      'process.env.FACEBOOK_APP': JSON.stringify('542508729547138')
    })
  ],
  output: {
    path: path.join(process.cwd(), conf.paths.dist),
    filename: '[name]-[hash].js'
  },
  entry: {
    app: `./${conf.path.src('index')}`,
    vendor: Object.keys(pkg.dependencies).filter(name => !['bootstrap', 'font-awesome', 'flag-icon-css'].includes(name))
  }
};
