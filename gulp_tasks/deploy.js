const path = require('path');
const gulp = require('gulp');
const conf = require('../conf/gulp.conf');
const deployPlugin = require('gulp-deploy-git');

gulp.task('deploy:config', deployConfig);
gulp.task('deploy:process', gulp.series('deploy:config', deploy));

/**
 * Prepare dist dir for deployment;
 * Add nginx conf and .env and .static file to dir.
 */
function deployConfig() {
  return gulp
    .src([
      path.join(conf.paths.conf, '/app-nginx.conf.sigil'),
      path.join(conf.paths.conf, '/.static')
    ])
    .pipe(gulp.dest(conf.paths.dist));
}

/**
 * Process to dokku deployment thanks to gulp module.
 */
function deploy() {
  return gulp
    .src(path.join(conf.paths.dist, '**/*'), {'dot': true, 'read': false})
    .pipe(deployPlugin({
      repository: 'dokku@footbar.com:mancity',
      prefix: 'dist',
      verbose: true
    }));
}
